package com.example.riosenjou.latihancevest.CameraApps;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.riosenjou.latihancevest.R;

public class ScanBarcode extends AppCompatActivity implements View.OnClickListener {

    private Button scanBtn,scanbtn2;
    private TextView formatTxt, contentTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_apps);

        scanbtn2=(Button)findViewById(R.id.scan_button2);
        scanBtn = (Button)findViewById(R.id.scan_button);
        formatTxt = (TextView)findViewById(R.id.scan_format);
        contentTxt = (TextView)findViewById(R.id.scan_content);

        scanBtn.setOnClickListener(this);
        scanbtn2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.scan_button){
            scanBarcode("PRODUCT_MODE");

        }else if (v.getId()==R.id.scan_button2){
            scanBarcode("QR_CODE_MODE");
        } else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void scanBarcode(String mode) {

        try {

            //buat intent untuk memanggil fungsi scan pada aplikasi zxing

            Intent intent = new Intent("com.google.zxing.client.android.SCAN");

            intent.putExtra("SCAN_MODE", mode); // "PRODUCT_MODE for bar codes

            startActivityForResult(intent, 1);

        } catch (Exception e) {


            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");

            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);

            startActivity(marketIntent);



        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {


                String contents = data.getStringExtra("SCAN_RESULT");

                Toast.makeText(getBaseContext(), "Hasil :"+contents, Toast.LENGTH_SHORT).show();


                contentTxt.setText("CONTENT: " + contents);

            }
        }



    }
}
