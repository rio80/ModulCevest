package com.example.riosenjou.latihancevest.SpinnerMySql.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.riosenjou.latihancevest.R;
import com.example.riosenjou.latihancevest.SpinnerMySql.data.dataSpinner;

import java.util.List;

/**
 * Created by rio senjou on 11/11/2017.
 */

public class adapterSpinner extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<dataSpinner> dataspinner;

    public adapterSpinner(Activity activity, List<dataSpinner> item) {
        this.activity = activity;
        this.dataspinner = item;
    }

    @Override
    public int getCount() {
        return dataspinner.size();
    }

    @Override
    public Object getItem(int position) {
        return dataspinner.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater==null){
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null){
            convertView=inflater.inflate(R.layout.list_spinner_pendidikan,null);
        }
        TextView pendidikan=(TextView)convertView.findViewById(R.id.pendidikan);
        dataSpinner item=dataspinner.get(position);
        pendidikan.setText(item.getPendidikan());
        return convertView;
    }
}
