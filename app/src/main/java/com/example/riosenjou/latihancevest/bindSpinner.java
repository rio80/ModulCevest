package com.example.riosenjou.latihancevest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.riosenjou.latihancevest.dbHandler.databaseHandler;

import java.util.List;

public class bindSpinner extends Activity implements
        AdapterView.OnItemSelectedListener {

    Spinner spinner;
    Button btnAdd;
    EditText inputLabel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_spinner);

        spinner=(Spinner)findViewById(R.id.spinner);
        btnAdd=(Button)findViewById(R.id.button_add);
        inputLabel=(EditText)findViewById(R.id.inputlabel);
        spinner.setOnItemSelectedListener(this);
        loadSpinnerData();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String label=inputLabel.getText().toString();
                if(label.trim().length()>0){
                    databaseHandler dbHandler=new databaseHandler(getApplicationContext());
                    dbHandler.insertLabel(label);
                    inputLabel.setText("");
                    InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(inputLabel.getWindowToken(),0);

                    loadSpinnerData();
                }else{
                    Toast.makeText(getApplicationContext(),"Please Enter Label Name",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void loadSpinnerData(){
        databaseHandler dbHandler=new databaseHandler(getApplicationContext());
        List<String> lables= dbHandler.getAllLabel();
        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>
                (this,android.R.layout.simple_spinner_item,lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String label=parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),"You Selected : "+label,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
