package com.example.riosenjou.latihancevest.polimorphism;

/**
 * Created by rio senjou on 22/10/2017.
 */

public class cacing extends Makhluk {
    public String jenis;
    public cacing(String jenis) {
        super("Cacing");
        this.jenis=jenis;
    }
    public String bernafas(){
        return "Bernafas dengan Kulit";
    }

    @Override
    public String toString() {
        return super.toString()+"\nDari jenis "+jenis;
    }
}
