package com.example.riosenjou.latihancevest.SpinnerMySql.data;

/**
 * Created by rio senjou on 11/11/2017.
 */

public class dataSpinner {
    private String id, pendidikan;

    public dataSpinner() {
    }

    public dataSpinner(String id, String pendidikan) {
        this.id = id;
        this.pendidikan = pendidikan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }
}
