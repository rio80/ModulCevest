package com.example.riosenjou.latihancevest;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.riosenjou.latihancevest.classRumus.rumus;

public class Rumus extends AppCompatActivity {
    int p,l,t,a;
    EditText panjang, lebar, tinggi, alas;
    Button hitungPersegi, hitungSegitiga;
    TextView hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rumus);
        init_rumus();
    }

    public void init_rumus() {


        panjang = (EditText) findViewById(R.id.edit_panjang);
        lebar = (EditText) findViewById(R.id.edit_lebar);
        tinggi = (EditText) findViewById(R.id.edit_tinggi);
        alas = (EditText) findViewById(R.id.edit_alas);

        hitungPersegi = (Button) findViewById(R.id.hitungPersegi);
        hitungSegitiga = (Button) findViewById(R.id.hitungSegitiga);

        hasil = (TextView) findViewById(R.id.hasilHitung);
        hitungPersegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    p= Integer.valueOf(panjang.getText().toString());
                    l= Integer.valueOf(lebar.getText().toString());
                    rumus rumusBangun = new rumus(p, l, 0, 0);
                    hasil.setText(""+rumusBangun.persegiPanjang());
                }catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(),e.getMessage().toString(),Toast.LENGTH_SHORT).show();
                }
            }
        });

        hitungSegitiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    t= Integer.valueOf(tinggi.getText().toString());
                    a= Integer.valueOf(alas.getText().toString());
                    rumus rumusSegitiga = new rumus(0, 0, t, a);
                    hasil.setText(""+rumusSegitiga.segitiga());
                }catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
