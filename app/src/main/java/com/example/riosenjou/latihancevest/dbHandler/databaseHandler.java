package com.example.riosenjou.latihancevest.dbHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rio senjou on 22/10/2017.
 */

public class databaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="SpinnerExample";
    private static final String TABLE_LABELS = "labels";

    // Labels Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    public databaseHandler(Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CATEGORY_TABLE="CREATE TABLE "+TABLE_LABELS + "("+
                KEY_ID+ " INTEGER PRIMARY KEY,"+KEY_NAME+" TEXT);";
        db.execSQL(CREATE_CATEGORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String SQL_UPGRADE="DROP TABLE IF EXISTS "+DATABASE_NAME;
        db.execSQL(SQL_UPGRADE);
        onCreate(db);
    }

    public void insertLabel(String label){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,label);
        db.insert(TABLE_LABELS,null,values);
        db.close();
    }

    public List<String> getAllLabel(){
        List<String> labels=new ArrayList<>();
        String selectQuery="SELECT * FROM "+TABLE_LABELS;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do{
                labels.add(cursor.getString(1));
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return labels;
    }
}
