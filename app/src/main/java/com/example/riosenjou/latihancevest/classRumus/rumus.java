package com.example.riosenjou.latihancevest.classRumus;

/**
 * Created by rio senjou on 25/10/2017.
 */

public class rumus {
    private int panjang;
    private int lebar;
    private int tinggi;
    private int alas;

    public rumus(int panjang, int lebar, int tinggi, int alas) {
        this.panjang = panjang;
        this.lebar = lebar;
        this.tinggi = tinggi;
        this.alas = alas;
    }

    public int persegiPanjang() {
        int hasil;
        hasil = panjang * lebar;
        return hasil;
    }

    public Double segitiga() {
        Double hasil;
        hasil = 1/2d * Double.valueOf(alas*tinggi) ;
        return hasil;
    }
}
