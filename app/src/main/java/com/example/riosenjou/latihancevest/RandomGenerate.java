package com.example.riosenjou.latihancevest;

import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

public class RandomGenerate extends AppCompatActivity {
    TextView textNama;
    ArrayList<String> nama;
    int getIndex;
    Button btnApprove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_generate);

        nama = new ArrayList<>();
        nama.add("Syafii");
        nama.add("Sarifullah");
        nama.add("Siddiq Putra Yoggi S");
        nama.add("Taufiq Maulana Abdullah");
        nama.add("Tri Widodo Sukarman");
        nama.add("Wira Suherman");
        nama.add("Alvian Dwipratama");
        nama.add("Bayu Putra");
        nama.add("Bella Salvina");
        nama.add("Chintia Jefrina");
        nama.add("Eni Ria Wati");
        nama.add("Ira Rahayu");
        nama.add("Mories Deo Hutapea");
        nama.add("Na'il Muzhaffar");
        nama.add("Rizkysea Fendra Mulya");
        nama.add("Rully Andrian");

        textNama = (TextView) findViewById(R.id.namaacak);
        btnApprove = (Button) findViewById(R.id.btnapprove);
        btnApprove.setEnabled(false);
    }

    public void acaknama(View view) {
        btnApprove.setEnabled(false);
        new doBackground().execute();
    }

    public void approve(View view) {
        btnApprove.setEnabled(false);
        nama.remove(getIndex);
    }

    class doBackground extends AsyncTask<Void, Integer, Void> {
        String tampungnama;
        private Random random;
        int progress_status;

        @Override
        protected Void doInBackground(Void... voids) {
            while (progress_status < 100) {
                int jumlahApprove, i = 0;
                progress_status += 2;
                random = new Random();
                while (i < nama.size()) {
                    getIndex = random.nextInt(nama.size());
                    tampungnama = nama.get(getIndex);
                    i++;
                }
                publishProgress(progress_status);
                SystemClock.sleep(150);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_status=0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            textNama.setText(tampungnama);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            textNama.setText(tampungnama);
            btnApprove.setEnabled(true);
        }


    }
}




