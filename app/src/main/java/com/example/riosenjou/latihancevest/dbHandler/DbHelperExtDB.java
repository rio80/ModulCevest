package com.example.riosenjou.latihancevest.dbHandler;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rio senjou on 05/11/2017.
 */

public class DbHelperExtDB extends SQLiteOpenHelper {
   private static final String DATABASE_NAME="DbImage.db";
    private static final int DATABASE_VERSION=1;
    public DbHelperExtDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, "/sdcard/"+DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase.openOrCreateDatabase("/sdcard/"+DATABASE_NAME,null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
