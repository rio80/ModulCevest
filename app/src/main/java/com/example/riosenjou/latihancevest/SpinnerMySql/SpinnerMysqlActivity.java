package com.example.riosenjou.latihancevest.SpinnerMySql;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.riosenjou.latihancevest.R;
import com.example.riosenjou.latihancevest.SpinnerMySql.adapter.adapterSpinner;
import com.example.riosenjou.latihancevest.SpinnerMySql.app.appControllerSpinner;
import com.example.riosenjou.latihancevest.SpinnerMySql.data.dataSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SpinnerMysqlActivity extends AppCompatActivity {


    TextView txt_hasil;
    Spinner spinner_pendidikan;
    ProgressDialog pDialog;
    adapterSpinner adapter;
    List<dataSpinner> listpendidikan = new ArrayList<dataSpinner>();
    public static final String URL = "http://192.168.72.2:81/tes_spinner/menu.php";
    private static final String TAG = SpinnerMysqlActivity.class.getSimpleName();
    public static final String TAG_ID = "Id";
    public static final String TAG_PENDIDIKAN = "pendidikan";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_mysql);

        txt_hasil = (TextView) findViewById(R.id.text_hasil);
        spinner_pendidikan = (Spinner) findViewById(R.id.spinner_pendidikan);

        spinner_pendidikan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txt_hasil.setText("Pendidikan Terakhir" + listpendidikan.get(position).getPendidikan());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter = new adapterSpinner(SpinnerMysqlActivity.this, listpendidikan);
        spinner_pendidikan.setAdapter(adapter);

        callData();
    }

    public void callData() {
        listpendidikan.clear();
        pDialog = new ProgressDialog(SpinnerMysqlActivity.this);
        pDialog.setCancelable(true);
        pDialog.setMessage("Loading...");
        showDialog();

        JsonArrayRequest jArr = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, response.toString());

                //Parsing JSON
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        dataSpinner item = new dataSpinner();
                        item.setId(obj.getString(TAG_ID));
                        item.setPendidikan(obj.getString(TAG_PENDIDIKAN));
                        listpendidikan.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                adapter.notifyDataSetChanged();
                hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e(TAG, "Error : " + error.getMessage());
                Toast.makeText(SpinnerMysqlActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });
        // Adding request to request queue
        appControllerSpinner.getInstance().addToRequestQueue(jArr);
    }

    private void hideDialog() {
        if (!pDialog.isShowing()) pDialog.show();
    }

    private void showDialog() {
        if (pDialog.isShowing()) pDialog.dismiss();
    }
}
