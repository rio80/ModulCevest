package com.example.riosenjou.latihancevest.polimorphism;

/**
 * Created by rio senjou on 22/10/2017.
 */

public class ikan extends Makhluk {
    public String jenis;

    public ikan(String jenis) {
        super("Ikan");
        this.jenis = jenis;
    }

    @Override
    public String toString() {
        return super.toString()+"\nDari Jenis "+jenis;
    }

    @Override
    public String bernafas() {
        return "Bernafas dengan Insang";
    }
}
