package com.example.riosenjou.latihancevest;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

public class tesProgress extends AppCompatActivity {
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu1){
            Toast.makeText(getApplicationContext(),"Menu 1",Toast.LENGTH_SHORT).show();
        }else if(item.getItemId()==R.id.menu2){
            Toast.makeText(getApplicationContext(),"Menu 2",Toast.LENGTH_SHORT).show();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_bar,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tes_progress);

        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        progressDialog=new ProgressDialog(this);

        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        final int totalTime=300;

        final Thread t=new Thread(){
            @Override
            public void run() {
                int jumpTime=0;
                while (jumpTime<totalTime){
                    try {
                        sleep(200);
                        jumpTime+=5;
                        progressDialog.setProgress(jumpTime);
                    }catch (InterruptedException e){
                        Toast.makeText(getApplicationContext(),e.getMessage().toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        t.start();
    }
}
