package com.example.riosenjou.latihancevest.polimorphism;

/**
 * Created by rio senjou on 22/10/2017.
 */

public class manusia extends Makhluk {
    public String jenis;

    public manusia(String jenis) {
        super("Manusia");
        this.jenis = jenis;
    }

    @Override
    public String toString() {
        return super.toString()+"\nDari jenis "+jenis;
    }

    @Override
    public String bernafas() {
        return "Bernafas dengan paru paru";
    }
}
