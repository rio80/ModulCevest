package com.example.riosenjou.latihancevest.CalcGame;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.riosenjou.latihancevest.R;

import java.util.Random;

public class CalcGame extends AppCompatActivity {
    TextView nilai;
    EditText hasil;
    Button cekHasil,acak;
    int tampungHasil;
    String tampungAngka1,tampungAngka2,tampung;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc_game);
        nilai=(TextView)findViewById(R.id.nilai);
        hasil=(EditText)findViewById(R.id.hasil);
        cekHasil=(Button)findViewById(R.id.cekhasil);
        acak=(Button)findViewById(R.id.acak);

        cekHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tampungHasilClick,tampungHasilHitung;
                tampungHasilClick=hasil.getText().toString();
                tampungHasilHitung=String.valueOf(tampungHasil);
                if(tampungHasilClick.equals(tampungHasilHitung)){
                    Toast.makeText(CalcGame.this, "Hasil Benar", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(CalcGame.this, "Salahh!! bisa ngitung gak sih!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        acak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new doInBackground().execute();
            }
        });


    }

    class doInBackground extends AsyncTask<Void,Integer,Void>{
        private Random random;
        int progress_status;
        int jumlah=100;

        @Override
        protected Void doInBackground(Void... voids) {
            while (progress_status<100){
                int jumlahApprove, i = 0;
                progress_status += 2;
                random = new Random();
                while (i < jumlah) {
                    tampungAngka1 = String.valueOf(random.nextInt(jumlah));
                    tampungAngka2 = String.valueOf(random.nextInt(jumlah));
                    i++;
                }
                publishProgress(progress_status);
                SystemClock.sleep(150);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_status=0;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            tampung=tampungAngka1+"\n"+tampungAngka2;
            nilai.setText(tampung);
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tampung=tampungAngka1+"\n"+tampungAngka2;
            nilai.setText(tampung+" +");
            tampungHasil=Integer.valueOf(tampungAngka1)+Integer.valueOf(tampungAngka2);
        }


    }
}
