package com.example.riosenjou.latihancevest;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.riosenjou.latihancevest.classRumus.rumus;
import com.example.riosenjou.latihancevest.polimorphism.Makhluk;
import com.example.riosenjou.latihancevest.polimorphism.cacing;
import com.example.riosenjou.latihancevest.polimorphism.ikan;
import com.example.riosenjou.latihancevest.polimorphism.manusia;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        Snackbar.make(findViewById(android.R.id.content), "Tes dulu snackbar", 100000)
                .setAction("Submit", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Ini muncul dari snackbar", Toast.LENGTH_SHORT).show();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_blue_dark))
                .show();
    }





    public void init() {
        Button button1, button2, button3, button4, button5;
        tv1 = (TextView) findViewById(R.id.tv1);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button1.setOnClickListener(myHandler);
        button2.setOnClickListener(myHandler);
        button3.setOnClickListener(myHandler);
        button4.setOnClickListener(myHandler);
        button5.setOnClickListener(myHandler);
    }

    View.OnClickListener myHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String pesan = null;
            if (view.getId() == R.id.button1) {
                pesan = "Ini dari Button1";
            } else if (view.getId() == R.id.button2) {
                pesan = "Ini dari Button2";
            } else if (view.getId() == R.id.button3) {
                randomMakhluk();
                pesan = "Random makhluk";
            } else if (view.getId() == R.id.button4) {
                bubbleSort();
                pesan = "Algoritma Bubble sort";
            } else if (view.getId() == R.id.button5) {
                Intent i = new Intent(getApplicationContext(), bindSpinner.class);
                startActivity(i);
                pesan = "Bind Spinner from SqlLite";
            }
            Toast.makeText(getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
        }
    };

    //Untuk membuat variabel dan method polimorpism
    public void randomMakhluk() {
        Makhluk[] makhluknya = {
                new cacing("tanah"),
                new ikan("laut"),
                new manusia("Mamalia")
        };

        Makhluk pilihan;
        Random pilih = new Random();
        for (int i = 0; i < 2; i++) {
            pilihan = makhluknya[pilih.nextInt(makhluknya.length)];
            tv1.setText("\nPilihan anda\n" + pilihan + "\n" + pilihan.bernafas());

        }
    }

    //Penerapan Algoritma BubbleSort
    public void bubbleSort() {
        String toTextView;
        //Array yang akan diurutkan datanya
        String[] buah = {
                "Mangga", "Apel", "Jambu", "Strawberry", "Sawo", "Rambutan", "Nanas"
        };
        String smt;
        toTextView = "Sebelum diurut : \n";
        for (int i = 0; i < buah.length; i++) {
            if (i < buah.length - 1) {
                toTextView += buah[i] + ", ";
            } else {
                toTextView += buah[i];
            }

        }


        toTextView += "\nSesudah diurut : \n";
        for (int i = 0; i < buah.length - 1; i++) {
            for (int j = 0; j < buah.length - 1; j++) {
                if (buah[j].compareTo(buah[j + 1]) > 0) {
                    smt = buah[j + 1];
                    buah[j + 1] = buah[j];
                    buah[j] = smt;
                }
            }
        }
        for (int i = 0; i < buah.length; i++) {
            if (i < buah.length - 1) {
                toTextView += buah[i] + ", ";
            } else {
                toTextView += buah[i];
            }
        }
        tv1.setText("" + toTextView + "\n");
    }
}